# TP 3

## 1. Présentation

Créez une nouvelle machine qui portera ce service DNS.

Dans cette section, on va monter un serveur DNS afin de se passer du fichier /etc/hosts entre  les diffrentes machines du parc.
L'outil de référence est bind, minimaliste mais très robuste et mature (c'est cette solution qui est derrière la plupart des DNS racines).
Nous allons pour cela avoir besoin d'écrire des fichiers de zone. Ces fichiers standard décrivent à quel IP associer quel nom d'hôte au sein d'un domaine.

## 2. Mise en place


```bash=
[root@ns1 ~]# vim /etc/sysconfig/network-scripts/ifcfg-ens34

TYPE=Ethernet
BOOTPROTO=static
NAME=ens34
DEVICE=ens34
ONBOOT=yes
IPADDR=10.55.55.14
NETMASK=255.255.255.0

[root@ns1 ~]# ifdown ens34
Device 'ens34' successfully disconnected.
[root@ns1 ~]# ifup ens34
Device 'ens34' successfully connected.
[root@ns1 ~]# ip a
3: ens34: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 00:0c:29:03:97:92 brd ff:ff:ff:ff:ff:ff
    inet 10.55.55.14/24 brd 10.55.55.255 scope global noprefixroute ens34
```

```bash=
[root@ns1 ~]# vim /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
10.55.55.14 tp3.cesi
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6


[root@ns1 ~]# vim /etc/hostname
ns1.tp3.cesi

[root@ns1 ~]# vim /etc/resolv.conf
search localdomain tp3.cesi                                                    nameserver 10.55.55.2
~ 
```


```bash=
[root@ns1 ~]# yum install epel-release
Installed:
  epel-release.noarch 0:7-11

Complete!
                     
```
```bash=
[root@ns1 ~]# yum install bind
Dependency Updated:
  bind-libs-lite.x86_64 32:9.11.4-26.P2.el7_9.3               bind-license.noarch 32:9.11.4-26.P2.el7_9.3

Complete!

[root@ns1 ~]# yum install bind-utils
Installed:
  bind-utils.x86_64 32:9.11.4-26.P2.el7_9.3

Complete!
```
```bash=
[root@ns1 etc]# vim /etc/named.conf

//
// named.conf
//
// Provided by Red Hat bind package to configure the ISC BIND named(8) DNS
// server as a caching only nameserver (as a localhost DNS resolver only).
//
// See /usr/share/doc/bind*/sample/ for example named configuration files.
//
// See the BIND Administrator's Reference Manual (ARM) for details about the
// configuration located in /usr/share/doc/bind-{version}/Bv9ARM.html

options {
	listen-on port 53 { 127.0.0.1; 10.55.55.14; };
	listen-on-v6 port 53 { ::1; };
	directory 	"/var/named";
	dump-file 	"/var/named/data/cache_dump.db";
	statistics-file "/var/named/data/named_stats.txt";
	memstatistics-file "/var/named/data/named_mem_stats.txt";
	recursing-file  "/var/named/data/named.recursing";
	secroots-file   "/var/named/data/named.secroots";
	allow-query     { 10.55.55.0/24; };

	/* 
	 - If you are building an AUTHORITATIVE DNS server, do NOT enable recursion.
	 - If you are building a RECURSIVE (caching) DNS server, you need to enable 
	   recursion. 
	 - If your recursive DNS server has a public IP address, you MUST enable access 
	   control to limit queries to your legitimate users. Failing to do so will
	   cause your server to become part of large scale DNS amplification 
	   attacks. Implementing BCP38 within your network would greatly
	   reduce such attack surface 
	*/
	recursion yes;

	dnssec-enable yes;
	dnssec-validation yes;

	/* Path to ISC DLV key */
	bindkeys-file "/etc/named.root.key";

	managed-keys-directory "/var/named/dynamic";

	pid-file "/run/named/named.pid";
	session-keyfile "/run/named/session.key";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "." IN {
	type hint;
	file "named.ca";
};

zone "tp3.cesi" IN {
         
         type master;
        
         file "/var/named/tp3.cesi.db";

         allow-update { none; };
};

zone "55.55.10.in-addr.arpa" IN {
          
          type master;
          
          file "/var/named/55.55.10.db";
         
          allow-update { none; };
};

include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";
```

```bash=
[root@ns1 etc]# cd /var/named
[root@ns1 named]# ls
data  dynamic  named.ca  named.empty  named.localhost  named.loopback  slaves
[root@ns1 named]# touch tp3.cesi.db
[root@ns1 named]# vim tp3.cesi.db

$TTL    604800                                                                                                          @  IN  SOA     ns1.tp3.cesi. root.tp3.cesi. (                                                                                                                           1001    ;Serial
                                                3H      ;Refresh                                                                                                        15M     ;Retry
                                                1W      ;Expire                                                                                                         1D      ;Minimum TTL                                                                                                    )                                                                                                                                                                                               ;Name Server Information                                                                                                @      IN  NS      ns1.tp3.cesi.                                                                                                                                                                                                                ns1 IN  A       10.55.55.14                                                                                             web     IN  A       10.55.55.11                                                                                         db     IN  A       10.55.55.12                                                                                          rp     IN  A       10.55.55.13
```

```bash=
[root@ns1 named]# vim 55.55.10.db

$TTL    604800
@   IN  SOA     ns1.tp3.cesi. root.tp3.cesi. (
                                                1001    ;Serial
                                                3H      ;Refresh
                                                15M     ;Retry
                                                1W      ;Expire
                                                1D      ;Minimum TTL
                                                )

;Name Server Information
@ IN  NS      ns1.tp3.cesi.

14.55.55.10 IN PTR ns1.tp3.cesi.

;PTR Record IP address to HostName
11      IN  PTR     web.tp3.cesi.
12      IN  PTR     db.tp3.cesi.
13      IN  PTR     rp.tp3.cesi.
```
```bash=
[root@ns1 named]# sudo firewall-cmd --add-port=53/tcp --permanent
sudo firewall-cmd --add-port=53/udp --permanentsuccess
[root@ns1 named]# firewall-cmd --add-port=53/udp --permanent
success
[root@ns1 named]# firewall-cmd --reload
success
[root@ns1 named]# systemctl start named
[root@ns1 named]# systemctl enable named
Created symlink from /etc/systemd/system/multi-user.target.wants/named.service to /usr/lib/systemd/system/named.service.
[root@ns1 named]# systemctl status named
● named.service - Berkeley Internet Name Domain (DNS)
   Loaded: loaded (/usr/lib/systemd/system/named.service; enabled; vendor preset: disabled)
   Active: active (running) since Fri 2020-12-18 10:40:45 CET; 8s ago
 Main PID: 2227 (named)
   CGroup: /system.slice/named.service
           └─2227 /usr/sbin/named -u named -c /etc/named.conf

Dec 18 10:40:45 ns1.tp2.cesi named[2227]: network unreachable resolving './DNSKEY/IN': 2001:500:1::53#53
Dec 18 10:40:45 ns1.tp2.cesi named[2227]: network unreachable resolving './NS/IN': 2001:500:1::53#53
Dec 18 10:40:45 ns1.tp2.cesi named[2227]: network unreachable resolving './DNSKEY/IN': 2001:500:200::b#53
Dec 18 10:40:45 ns1.tp2.cesi named[2227]: network unreachable resolving './NS/IN': 2001:500:200::b#53
Dec 18 10:40:45 ns1.tp2.cesi named[2227]: network unreachable resolving './DNSKEY/IN': 2001:500:9f::42#53
Dec 18 10:40:45 ns1.tp2.cesi named[2227]: network unreachable resolving './NS/IN': 2001:500:9f::42#53
Dec 18 10:40:45 ns1.tp2.cesi named[2227]: network unreachable resolving './DNSKEY/IN': 2001:7fe::53#53
Dec 18 10:40:45 ns1.tp2.cesi named[2227]: network unreachable resolving './NS/IN': 2001:7fe::53#53
Dec 18 10:40:45 ns1.tp2.cesi named[2227]: managed-keys-zone: Key 20326 for zone . acceptance timer complete: key...usted
Dec 18 10:40:45 ns1.tp2.cesi named[2227]: resolver priming query complete
Hint: Some lines were ellipsized, use -l to show in full.
```
# 3. Test 
```bash=
[root@db ~]# yum install bind-utils
  bind-libs.x86_64 32:9.11.4-26.P2.el7_9.2

Complete!
You have new mail in /var/spool/mail/root
```
```bash=
[root@db ~]# dig web.tp3.cesi @10.55.55.14

; <<>> DiG 9.11.4-P2-RedHat-9.11.4-26.P2.el7_9.2 <<>> web.tp3.cesi @10.55.55.14
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 23241
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 2

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;web.tp3.cesi.                  IN      A

;; ANSWER SECTION:
web.tp3.cesi.           604800  IN      A       10.55.55.11

;; AUTHORITY SECTION:
tp3.cesi.               604800  IN      NS      ns1.tp3.cesi.

;; ADDITIONAL SECTION:
ns1.tp3.cesi.           604800  IN      A       10.55.55.14

;; Query time: 0 msec
;; SERVER: 10.55.55.14#53(10.55.55.14)
;; WHEN: Fri Dec 18 10:44:23 CET 2020
;; MSG SIZE  rcvd: 91
```
```bash=
[root@db ~]# dig -x 10.55.55.13 @10.55.55.14

; <<>> DiG 9.11.4-P2-RedHat-9.11.4-26.P2.el7_9.2 <<>> -x 10.55.55.13 @10.55.55.14
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 21204
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 2

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;13.55.55.10.in-addr.arpa.      IN      PTR

;; ANSWER SECTION:
13.55.55.10.in-addr.arpa. 604800 IN     PTR     rp.tp3.cesi.

;; AUTHORITY SECTION:
55.55.10.in-addr.arpa.  604800  IN      NS      ns1.tp3.cesi.

;; ADDITIONAL SECTION:
ns1.tp3.cesi.           604800  IN      A       10.55.55.14

;; Query time: 0 msec
;; SERVER: 10.55.55.14#53(10.55.55.14)
;; WHEN: Fri Dec 18 10:45:13 CET 2020
;; MSG SIZE  rcvd: 112

```
## 4. Configuration du DNS de façon permanente
```bash=
[root@db ~]# vim /etc/resolv.conf
# Generated by NetworkManager
search localdomain tp3.cesi
nameserver 10.55.55.14                                                                                                  ~                      
```

# II. Backup

```bash=
[root@web ~]# yum install rsync
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: miroir.univ-lorraine.fr
 * epel: fr2.rpmfind.net
 * extras: centos.quelquesmots.fr
 * remi-php56: fr2.rpmfind.net
 * remi-safe: fr2.rpmfind.net
 * updates: centos.quelquesmots.fr
Package rsync-3.1.2-10.el7.x86_64 already installed and latest version
Nothing to do
[root@web ~]# yum install shhd
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: mirror.in2p3.fr
 * epel: mirror.neostrada.nl
 * extras: centos.quelquesmots.fr
 * remi-php56: remirepo.reloumirrors.net
 * remi-safe: remirepo.reloumirrors.net
 * updates: centos.quelquesmots.fr
No package shhd available.
Error: Nothing to do
```
```bash=
[root@web ~]# cd /opt
[root@web opt]# ls
[root@web opt]# ls -al
total 0
drwxr-xr-x.  2 root root   6 Apr 11  2018 .
dr-xr-xr-x. 17 root root 242 Dec 18 09:27 ..
[root@web opt]# mkdir /opt/backup
[root@web opt]# ls -al
total 0
drwxr-xr-x.  3 root root  20 Dec 18 11:01 .
dr-xr-xr-x. 17 root root 242 Dec 18 09:27 ..
drwxr-xr-x.  2 root root   6 Dec 18 11:01 backup

[root@web opt]# touch backup.sh
[root@web opt]# vim backup.sh

#!/bin/bash
#Var
dir_to_backup="/var/www/html"
backup_path="/opt/backup/"
backup_name="Wordpress.$(date +%F%H-%M).tar.gz"


rotate () {
   rm -f $(ls -1t $backup_path | tail -n +8)
   rsync -av --delete -e 'ssh -p 22' $backup_path admin@10.55.55.13:$backup_path
}

backup () {
  tar -czpvf $(echo $backup_path$backup_name) -C $dir_to_backup .
}

backup
rotate
```
```bash=
[root@web backup]# bash backup.sh
admin@10.55.55.13's password:
sending incremental file list
./
Wordpress.2020-12-1815-39.tar.gz

sent 15,336,166 bytes  received 38 bytes  2,788,400.73 bytes/sec
total size is 15,332,289  speedup is 1.00
```
```bash=
[root@web backup]# ls
Wordpress.2020-12-1815-39.tar.gz

[root@rp backup]# ls
Wordpress.2020-12-1815-39.tar.gz

```

```bash=
[root@web backup]# cd /etc/systemd/system/
[root@web system]# ls
basic.target.wants                           default.target.wants         sockets.target.wants
dbus-org.fedoraproject.FirewallD1.service    getty.target.wants           sysinit.target.wants
dbus-org.freedesktop.NetworkManager.service  local-fs.target.wants        system-update.target.wants
dbus-org.freedesktop.nm-dispatcher.service   multi-user.target.wants      vmtoolsd.service.requires
default.target                               network-online.target.wants  web.service
[root@web system]# vim backup.timer
[Unit]
Description=Run backup.service periodically

[Timer]
# Everyday at 04:00
OnCalendar=*-*-* 4:00:00

[Install]
WantedBy=multi-user.target
```

```bash=
[root@web system]# vim backup.service

description=Very simple web service

[Service]
ExecStart=/bin/bash backup.sh
user=root
WorkingDirectory=/opt/

[Install]
WantedBy=multi-user.target

[root@web system]# # systemctl daemon-reload
[root@web system]# systemctl start backup.service
[root@web system]# systemctl status backup.service
```

```bash=

[root@web system]# ls /etc/systemd/system | grep backup
backup.service
backup.timer

[root@web system]# systemctl disable backup.service
[root@web system]# systemctl enable backup.timer
[root@web system]# systemctl start backup.timer

Sat 2020-12-19 04:00:00 CET  11h left n/a                          n/a    backup.timer                 backup.service
Sat 2020-12-19 08:44:55 CET  16h left Fri 2020-12-18 08:44:55 CET  7h ago systemd-tmpfiles-clean.timer systemd-tmpfiles-

```

# TP 2 : Serveur Web 

## O . Prérequis 

###### COMS: `On va s'occuper de modifier la configuration réseau pour que nos 3 Machines communiquent. Les mêmes étapes seront faites pour les 2 autres machines` 
```bash=
[root@localhost ~]# vim /etc/sysconfig/network-scripts/ifcfg-ens34
```
###### COMS: `On modifie l'adresse ip en 10.55.55.11`
```bash=
TYPE=Ethernet
BOOTPROTO=static
NAME=ens34
DEVICE=ens34
ONBOOT=yes
IPADDR=10.55.55.11
NETMASK=255.255.255.0
```
###### COMS: `On doit désactiver la carte réseau ens 34 et la réactiver pour qu'elle prenne sa nouvelle configuration, ensuite on fait un ip a pour vérifier qu'elle a bien sa nouvelle ip`
```bash=
[root@localhost ~]# ifdown ens34
Device 'ens34' successfully disconnected.
[root@localhost ~]# ifup ens34
Device 'ens34' successfully connected.
```

```bash=
[root@localhost ~]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: ens33: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 00:0c:29:2d:ea:77 brd ff:ff:ff:ff:ff:ff
    inet 10.55.55.130/24 brd 10.55.55.255 scope global noprefixroute dynamic ens33
       valid_lft 1185sec preferred_lft 1185sec
    inet6 fe80::e17c:baee:777d:d99f/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: ens34: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 00:0c:29:2d:ea:81 brd ff:ff:ff:ff:ff:ff
    inet 10.55.55.11/24 brd 10.55.55.255 scope global noprefixroute ens34
       valid_lft forever preferred_lft forever
    inet6 fe80::20c:29ff:fe2d:ea81/64 scope link
       valid_lft forever preferred_lft forever
```

###### COMS: `On a changé les adresses Ip de nos autres machines, le Serveur Web en 10.55.55.12 et le REVERSE Proxy en .13 `
```bash=
[root@localhost ~]# ping 10.55.55.12
PING 10.55.55.12 (10.55.55.12) 56(84) bytes of data.
64 bytes from 10.55.55.12: icmp_seq=1 ttl=64 time=1.00 ms
64 bytes from 10.55.55.12: icmp_seq=2 ttl=64 time=0.201 ms
64 bytes from 10.55.55.12: icmp_seq=3 ttl=64 time=0.205 ms
^C
--- 10.55.55.12 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2001ms
rtt min/avg/max/mdev = 0.201/0.469/1.001/0.376 ms
[root@localhost ~]# ping 10.55.55.13
PING 10.55.55.13 (10.55.55.13) 56(84) bytes of data.
64 bytes from 10.55.55.13: icmp_seq=1 ttl=64 time=0.311 ms
64 bytes from 10.55.55.13: icmp_seq=2 ttl=64 time=0.196 ms
64 bytes from 10.55.55.13: icmp_seq=3 ttl=64 time=0.191 ms
^C
--- 10.55.55.13 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2000ms
rtt min/avg/max/mdev = 0.191/0.232/0.311/0.058 ms
```

###### COMS: `On va changer le nom de domaine de notre machine dans le fichier hosts` 


```bash=
[root@localhost ~]# vim /etc/hosts

127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
10.55.55.11 tp2.cesi
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6                                         ~       
```
###### COMS: `On vérifie qu'on ping bien notre domaine`

```bash=
[root@db ~]# ping tp2.cesi
PING tp2.cesi (10.55.55.11) 56(84) bytes of data.
64 bytes from tp2.cesi (10.55.55.11): icmp_seq=1 ttl=64 time=0.034 ms
64 bytes from tp2.cesi (10.55.55.11): icmp_seq=2 ttl=64 time=0.042 ms
64 bytes from tp2.cesi (10.55.55.11): icmp_seq=3 ttl=64 time=0.043 ms
^C
--- tp2.cesi ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 1999ms
rtt min/avg/max/mdev = 0.034/0.039/0.043/0.008 ms
```

###### COMS: `On change le hostname de la machine en db, on reboot la machine et on voit bien que le nom à changé`

```bash=
[root@localhost ~]# vim /etc/hostname
- - - 
db.tp2.cesi
- - -
[root@localhost ~]# reboot
Connection to 10.55.55.11 closed by remote host.
Connection to 10.55.55.11 closed.

Last login: Wed Dec 16 09:59:57 2020 from 10.55.55.1
[root@db ~]#

```

# 1 - Base de donnée

###### COMS: `On installe mariadb sur notre serveur de BDD, on confirme l'installation avec Y`

```bash=
[root@db ~]# yum install mariadb-server
- - -
Dependency Installed:
  mariadb.x86_64 1:5.5.68-1.el7                             perl-Compress-Raw-Bzip2.x86_64 0:2.061-3.el7
  perl-Compress-Raw-Zlib.x86_64 1:2.061-4.el7               perl-DBD-MySQL.x86_64 0:4.023-6.el7
  perl-DBI.x86_64 0:1.627-4.el7                             perl-Data-Dumper.x86_64 0:2.145-3.el7
  perl-IO-Compress.noarch 0:2.061-2.el7                     perl-Net-Daemon.noarch 0:0.48-5.el7
  perl-PlRPC.noarch 0:0.2020-14.el7

Complete!
- - -
```

###### COMS: `Une fois MariaDb installée, il faut demander à systemd de relire les fichiers de configuration + démarré Mariadb et on vérifie que le service est bien démarré avec status; MariaDB a démarré avec succès, la sortie affiche active (running)` 
```bash= 
[root@db ~]# systemctl daemon-reload
[root@db ~]# systemctl start mariadb
[root@db ~]# systemctl status mariadb
● mariadb.service - MariaDB database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; disabled; vendor preset: disabled)
   Active: active (running) since Wed 2020-12-16 10:45:58 CET; 11s ago
  Process: 1600 ExecStartPost=/usr/libexec/mariadb-wait-ready $MAINPID (code=exited, status=0/SUCCESS)
  Process: 1517 ExecStartPre=/usr/libexec/mariadb-prepare-db-dir %n (code=exited, status=0/SUCCESS)
 Main PID: 1599 (mysqld_safe)
   CGroup: /system.slice/mariadb.service
           ├─1599 /bin/sh /usr/bin/mysqld_safe --basedir=/usr
           └─1764 /usr/libexec/mysqld --basedir=/usr --datadir=/var/lib/mysql --plugin-dir=/usr/lib64/mysql/plugin -...
```

###### COMS: `On s'assure que Mariadb se lance au démarrage avec la commande enable`
```bash=
[root@db ~]# systemctl enable mariadb
Created symlink from /etc/systemd/system/multi-user.target.wants/mariadb.service to /usr/lib/systemd/system/mariadb.service.
```

###### COMS: `On lance un script de sécursiation de la bdd mariadb, on fait bien attention à définir le mot de passe root `

```bash=
[root@db ~]# mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user.  If you've just installed MariaDB, and
you haven't set the root password yet, the password will be blank,
so you should just press enter here.

Enter current password for root (enter for none):
ERROR 1045 (28000): Access denied for user 'root'@'localhost' (using password: YES)
Enter current password for root (enter for none):
ERROR 1045 (28000): Access denied for user 'root'@'localhost' (using password: YES)
Enter current password for root (enter for none):
ERROR 1045 (28000): Access denied for user 'root'@'localhost' (using password: YES)
Enter current password for root (enter for none):
ERROR 1045 (28000): Access denied for user 'root'@'localhost' (using password: YES)
Enter current password for root (enter for none):
ERROR 1045 (28000): Access denied for user 'root'@'localhost' (using password: YES)
Enter current password for root (enter for none):
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

Set root password? [Y/n] Y
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!


By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] n
 ... skipping.

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] n
 ... skipping.

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] n
 ... skipping.

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] Y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!

```
###### COMS: `On vérifie que l'installation est bonne en se connectant à l'outil mysqladmin, on se connecte en tant que root avec -u root et on exige le mot de passe avec -p avec la version de l'installation`

```bash=
[root@db ~]# mysqladmin -u root -p version
Enter password:
mysqladmin  Ver 9.0 Distrib 5.5.68-MariaDB, for Linux on x86_64
Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Server version          5.5.68-MariaDB
Protocol version        10
Connection              Localhost via UNIX socket
UNIX socket             /var/lib/mysql/mysql.sock
Uptime:                 40 min 6 sec

Threads: 1  Questions: 13  Slow queries: 0  Opens: 0  Flush tables: 2  Open tables: 26  Queries per second avg: 0.005
```

###### COMS: `On se connecte à Mariadb`
```bash=
[root@db ~]# mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 13
Server version: 5.5.68-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
```
###### COMS: `On va créer la BDD wordpress avec la commande ci-dessous et on précise bien qu'on l'utilise avec use`
```bash=
MariaDB [(none)]> create database if not exists wordpress;
Query OK, 1 row affected (0.00 sec)

MariaDB [(none)]> use wordpress
Database changed
```
###### COMS: `On vérifie que la database est bien crée`
```bash=
MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| test               |
| wordpress          |
+--------------------+
```

###### COMS: `On va créer notre utilisateur nommé toto`
```bash=
MariaDB [(none)]> create user 'toto@10.55.55.12' identified by 'toto';
Query OK, 0 rows affected (0.00 sec)
```

###### COMS: `On attribue les droits à l'utilisateur labweb sur la base de donnée wordpress`
```bash=
MariaDB [(none)]> grant all privileges on wordpress.* to 'toto@10.55.55.12' identified by 'toto';
Query OK, 0 rows affected (0.00 sec)
```
###### COMS: `On refresh les privilèges avec la commande flush`
```bash=
MariaDB [wordpress]> flush privileges;
Query OK, 0 rows affected (0.00 sec)
```

###### COMS: `On vérifie que que l'utilisateur est bien crée avec la commande select user pointant sur mysql`
```bash=
MariaDB [wordpress]> select user from mysql.user;
+------------------+
| user             |
+------------------+
| toto@10.55.55.12 |
| root             |
| root             |
|                  |
| root             |
|                  |
| toto             |
| root             |
+------------------+
8 rows in set (0.00 sec)
```

###### COMS: `On vérifie que l'utilisateur est bien crée avec les bons droits avec la commande ci-dessous`
```bash=
MariaDB [(none)]>  select * from mysql.user;
+-------------+------------------+-------------------------------------------+-------------+-------------+-------------+-------------+-------------+-----------+-------------+---------------+--------------+-----------+------------+-----------------+------------+------------+--------------+------------+-----------------------+------------------+--------------+-----------------+------------------+------------------+----------------+---------------------+--------------------+------------------+------------+--------------+------------------------+----------+------------+-------------+--------------+---------------+-------------+-----------------+----------------------+--------+-----------------------+
| Host        | User             | Password                                  | Select_priv | Insert_priv | Update_priv | Delete_priv | Create_priv | Drop_priv | Reload_priv | Shutdown_priv | Process_priv | File_priv | Grant_priv | References_priv | Index_priv | Alter_priv | Show_db_priv | Super_priv | Create_tmp_table_priv | Lock_tables_priv | Execute_priv | Repl_slave_priv | Repl_client_priv | Create_view_priv | Show_view_priv | Create_routine_priv | Alter_routine_priv | Create_user_priv | Event_priv | Trigger_priv | Create_tablespace_priv | ssl_type | ssl_cipher | x509_issuer | x509_subject | max_questions | max_updates | max_connections | max_user_connections | plugin | authentication_string |
+-------------+------------------+-------------------------------------------+-------------+-------------+-------------+-------------+-------------+-----------+-------------+---------------+--------------+-----------+------------+-----------------+------------+------------+--------------+------------+-----------------------+------------------+--------------+-----------------+------------------+------------------+----------------+---------------------+--------------------+------------------+------------+--------------+------------------------+----------+------------+-------------+--------------+---------------+-------------+-----------------+----------------------+--------+-----------------------+
| localhost   | root             | *B4EF459E79C9391DD92B0FC14C7F5BCD8BE47D67 | Y           | Y           | Y           | Y           | Y           | Y         | Y           | Y             | Y            | Y         | Y          | Y               | Y          | Y          | Y            | Y          | Y                     | Y                | Y            | Y               | Y                | Y                | Y              | Y                   | Y                  | Y                | Y          | Y            | Y                      |          |            |             |              |             0 |           0 |               0 |                    0 |        |                       |
| db.tp2.cesi | root             | *B4EF459E79C9391DD92B0FC14C7F5BCD8BE47D67 | Y           | Y           | Y           | Y           | Y           | Y         | Y           | Y             | Y            | Y         | Y          | Y               | Y          | Y          | Y            | Y          | Y                     | Y                | Y            | Y               | Y                | Y                | Y              | Y                   | Y                  | Y                | Y          | Y            | Y                      |          |            |             |              |             0 |           0 |               0 |                    0 |        |                       |
| 127.0.0.1   | root             | *B4EF459E79C9391DD92B0FC14C7F5BCD8BE47D67 | Y           | Y           | Y           | Y           | Y           | Y         | Y           | Y             | Y            | Y         | Y          | Y               | Y          | Y          | Y            | Y          | Y                     | Y                | Y            | Y               | Y                | Y                | Y              | Y                   | Y                  | Y                | Y          | Y            | Y                      |          |            |             |              |             0 |           0 |               0 |                    0 |        |                       |
| ::1         | root             | *B4EF459E79C9391DD92B0FC14C7F5BCD8BE47D67 | Y           | Y           | Y           | Y           | Y           | Y         | Y           | Y             | Y            | Y         | Y          | Y               | Y          | Y          | Y            | Y          | Y                     | Y                | Y            | Y               | Y                | Y                | Y              | Y                   | Y                  | Y                | Y          | Y            | Y                      |          |            |             |              |             0 |           0 |               0 |                    0 |        |                       |
| localhost   |                  |                                           | N           | N           | N           | N           | N           | N         | N           | N             | N            | N         | N          | N               | N          | N          | N            | N          | N                     | N                | N            | N               | N                | N                | N              | N                   | N                  | N                | N          | N            | N                      |          |            |             |              |             0 |           0 |               0 |                    0 |        |                       |
| db.tp2.cesi |                  |                                           | N           | N           | N           | N           | N           | N         | N           | N             | N            | N         | N          | N               | N          | N          | N            | N          | N                     | N                | N            | N               | N                | N                | N              | N                   | N                  | N                | N          | N            | N                      |          |            |             |              |             0 |           0 |               0 |                    0 |        |                       |
| %           | toto@10.55.55.12 | *63D85DCA15EAFFC58C908FD2FAE50CCBC60C4EA2 | N           | N           | N           | N           | N           | N         | N           | N             | N            | N         | N          | N               | N          | N          | N            | N          | N                     | N                | N            | N               | N                | N                | N              | N                   | N                  | N                | N          | N            | N                      |          |            |             |              |             0 |           0 |               0 |                    0 |        |                       |
| localhost   | labweb           | *CCB212FC431546E38A8AA17697A28C6A399A44A8 | N           | N           | N           | N           | N           | N         | N           | N             | N            | N         | N          | N               | N          | N          | N            | N          | N                     | N                | N            | N               | N                | N                | N              | N                   | N                  | N                | N          | N            | N                      |          |            |             |              |             0 |           0 |               0 |                    0 |        |                       |
| 10.55.55.12 | toto             | *63D85DCA15EAFFC58C908FD2FAE50CCBC60C4EA2 | N           | N           | N           | N           | N           | N         | N           | N             | N            | N         | N          | N               | N          | N          | N            | N          | N                     | N                | N            | N               | N                | N                | N              | N                   | N                  | N                | N          | N            | N                      |          |            |             |              |             0 |           0 |               0 |                    0 |        |                       |
+-------------+------------------+-------------------------------------------+-------------+-------------+-------------+-------------+-------------+-----------+-------------+---------------+--------------+-----------+------------+-----------------+------------+------------+--------------+------------+-----------------------+------------------+--------------+-----------------+------------------+------------------+----------------+---------------------+--------------------+------------------+------------+--------------+------------------------+----------+------------+-------------+--------------+---------------+-------------+-----------------+----------------------+--------+-----------------------+
9 rows in set (0.00 sec)
```

###### COMS: `On sort de la base pour checker quel port utilise la base avec la commande ss`
```bash=
[root@db ~]# ss -alnpt
State       Recv-Q Send-Q                                                                                                                                        Local Address:Port                                                                                                                                                       Peer Address:Port
LISTEN      0      128                                                                                                                                                       *:22                                                                                                                                                                    *:*                   users:(("sshd",pid=1169,fd=3))
LISTEN      0      5                                                                                                                                                         *:8888                                                                                                                                                                  *:*                   users:(("python2",pid=760,fd=3))
LISTEN      0      100                                                                                                                                               127.0.0.1:25                                                                                                                                                                    *:*                   users:(("master",pid=1330,fd=13))
LISTEN      0      50                                                                                                                                                        *:3306                                                                                                                                                                  *:*                   users:(("mysqld",pid=1764,fd=14))
LISTEN      0      128                                                                                                                                                    [::]:22                                                                                                                                                                 [::]:*                   users:(("sshd",pid=1169,fd=4))
``` 

###### COMS: `Enfin on ouvre le port 3306 pour l'accès à la base et on relance le service` 
```bash=
[root@db ~]# firewall-cmd --add-port=3306/tcp --permanent
success
[root@db ~]# firewall-cmd --reload
success
```

# 2 - Serveur Web

###### COMS: `On se connecte à notre machine` 
```bash=
[root@web ~]#
```

## Installer le serveur
```bash=
[root@web ~]# yum update httpd
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: centos.crazyfrogs.org
 * extras: centos.quelquesmots.fr
 * updates: centos.quelquesmots.fr
base                                                                                                                                                                                                                                                    | 3.6 kB  00:00:00
extras                                                                                                                                                                                                                                                  | 2.9 kB  00:00:00
updates                                                                                                                                                                                                                                                 | 2.9 kB  00:00:00
Package(s) httpd available, but not installed.
No packages marked for update
```
```bash=
[root@web ~]# yum install httpd
Installed:
  httpd.x86_64 0:2.4.6-97.el7.centos

Dependency Installed:
  apr.x86_64 0:1.4.8-7.el7                                     apr-util.x86_64 0:1.5.2-6.el7                                     httpd-tools.x86_64 0:2.4.6-97.el7.centos                                     mailcap.noarch 0:2.1.41-2.el7

Complete!
```

```bash=
[root@web ~]# firewall-cmd --permanent --add-service=http
success
[root@web ~]# firewall-cmd --reload
success
```
```bash=
[root@web ~]# systemctl start httpd
[root@web ~]# systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
   Active: active (running) since Wed 2020-12-16 13:40:20 CET; 6s ago
     Docs: man:httpd(8)
           man:apachectl(8)
 Main PID: 1926 (httpd)
   Status: "Processing requests..."
   CGroup: /system.slice/httpd.service
           ├─1926 /usr/sbin/httpd -DFOREGROUND
           ├─1927 /usr/sbin/httpd -DFOREGROUND
           ├─1928 /usr/sbin/httpd -DFOREGROUND
           ├─1929 /usr/sbin/httpd -DFOREGROUND
           ├─1930 /usr/sbin/httpd -DFOREGROUND
           └─1931 /usr/sbin/httpd -DFOREGROUND

Dec 16 13:40:05 web.tp2.cesi systemd[1]: Starting The Apache HTTP Server...
Dec 16 13:40:20 web.tp2.cesi systemd[1]: Started The Apache HTTP Server.

[root@web ~]# systemctl enable httpd
Created symlink from /etc/systemd/system/multi-user.target.wants/httpd.service to /usr/lib/systemd/system/httpd.service.
```
![](https://i.imgur.com/zHjCcQx.png)

```bash=
[root@web ~]# firewall-cmd --permanent --add-service=https
success
[root@web ~]# firewall-cmd --reload
success
```


## Télécharger Wordpress

```bash=
curl https://wordpress.org/latest.tar.gz --output wordpress_latest.tar.gz
```

## extraire l'archive wordpress dans un dossier qui pourra être servi par Apache
```bash=
[root@web home]# tar -xzvf wordpress_latest.tar.gz
[root@web ~]# ls
wordpress  wordpress_latest.tar.gz
```
```bash=
[root@web home]# yum install rsync

  Installing : rsync-3.1.2-10.el7.x86_64                                                                                                                                                                                                                                   1/1
  Verifying  : rsync-3.1.2-10.el7.x86_64                                                                                                                                                                                                                                   1/1

Installed:
  rsync.x86_64 0:3.1.2-10.el7

Complete!
``` 

```bash=
[root@web ~]# rsync -avP wordpress/ /var/www/html/

[root@web ~]# mkdir /var/www/html/wp-content/uploads

[root@web ~]# cd /var/www/html/wp-content/
[root@web wp-content]# ls
index.php  plugins  themes  uploads


[root@web wp-content]# chown -R apache:apache /var/www/html/*

[root@web www]# ls -la
total 4
drwxr-xr-x.  4 root   root    33 Dec 16 13:38 .
drwxr-xr-x. 20 root   root   278 Dec 16 13:38 ..
drwxr-xr-x.  2 root   root     6 Nov 16 17:19 cgi-bin
drwxr-xr-x.  5 nobody 65534 4096 Dec  8 23:13 html

[root@web /]# cd /var/www/html
[root@web html]# ls -al
total 224
drwxr-xr-x.  5 nobody  65534  4096 Dec  8 23:13 .
drwxr-xr-x.  4 root   root      33 Dec 16 13:38 ..
-rw-r--r--.  1 apache apache   405 Feb  6  2020 index.php
-rw-r--r--.  1 apache apache 19915 Feb 12  2020 license.txt
-rw-r--r--.  1 apache apache  7278 Jun 26 15:58 readme.html
-rw-r--r--.  1 apache apache  7101 Jul 28 19:20 wp-activate.php
drwxr-xr-x.  9 apache apache  4096 Dec  8 23:13 wp-admin
-rw-r--r--.  1 apache apache   351 Feb  6  2020 wp-blog-header.php
-rw-r--r--.  1 apache apache  2328 Oct  8 23:15 wp-comments-post.php
-rw-r--r--.  1 apache apache  2913 Feb  6  2020 wp-config-sample.php
drwxr-xr-x.  5 apache apache    67 Dec 16 14:12 wp-content
-rw-r--r--.  1 apache apache  3939 Jul 30 21:14 wp-cron.php
drwxr-xr-x. 25 apache apache 12288 Dec  8 23:13 wp-includes
-rw-r--r--.  1 apache apache  2496 Feb  6  2020 wp-links-opml.php
-rw-r--r--.  1 apache apache  3300 Feb  6  2020 wp-load.php
-rw-r--r--.  1 apache apache 49831 Nov  9 11:53 wp-login.php
-rw-r--r--.  1 apache apache  8509 Apr 14  2020 wp-mail.php
-rw-r--r--.  1 apache apache 20975 Nov 12 15:43 wp-settings.php
-rw-r--r--.  1 apache apache 31337 Sep 30 23:54 wp-signup.php
-rw-r--r--.  1 apache apache  4747 Oct  8 23:15 wp-trackback.php
-rw-r--r--.  1 apache apache  3236 Jun  8  2020 xmlrpc.php

[root@web html]# cp wp-config-sample.php wp-config.php

``` 

![](https://i.imgur.com/v9Vg3Z8.png)


## Installation de dépôt additionels
```bash=
[root@web html]# yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm -y 
[root@web html]# yum install -y yum-utils 
```
## On supprime d'éventuelles vieilles versions de PHP précédemment installées
```bash=
[root@web html]# yum remove -y php 
```
## Activation du nouveau dépôt
```bash=
[root@web html]# yum-config-manager --enable remi-php56   
```
## Installation de PHP 5.6.40 et de librairies récurrentes
```bash=
[root@web html]# yum install php php-mcrypt php-cli php-gd php-curl php-mysql php-ldap php-zip php-fileinfo -y  
```
## accéder à l'interface de Wordpress afin de valider la bonne installation de la solution

![](https://i.imgur.com/0q6cRsi.png)



# 3 - REVERSE PROXY 

## Installer un serveur nginx 

```bash=
[root@rp ~]# yum -y update
```
###### COMS:`On installe EPEL repository car Nginx n'est pas présent dans le répository standard qui vient avec la version de CentOS` 
```bash=
[root@rp ~]# yum install -y epel-release

Running transaction
  Installing : epel-release-7-11.noarch                                                                                                                                                                                                                                    1/1
  Verifying  : epel-release-7-11.noarch                                                                                                                                                                                                                                    1/1

Installed:
  epel-release.noarch 0:7-11

Complete!
```

###### COMS: `On installe nginx sur le serveur`
```bash=
[root@rp ~]# yum –y install nginx

Installed:
  nginx.x86_64 1:1.16.1-3.el7

Dependency Installed:
  centos-indexhtml.noarch 0:7-9.el7.centos  dejavu-fonts-common.noarch 0:2.33-6.el7  dejavu-sans-fonts.noarch 0:2.33-6.el7   fontconfig.x86_64 0:2.13.0-4.3.el7                 fontpackages-filesystem.noarch 0:1.44-8.el7  gd.x86_64 0:2.0.35-26.el7
  gperftools-libs.x86_64 0:2.6.1-1.el7      libX11.x86_64 0:1.6.7-3.el7_9            libX11-common.noarch 0:1.6.7-3.el7_9    libXau.x86_64 0:1.0.8-2.1.el7                      libXpm.x86_64 0:3.5.12-1.el7                 libjpeg-turbo.x86_64 0:1.2.90-8.el7
  libxcb.x86_64 0:1.13-1.el7                nginx-all-modules.noarch 1:1.16.1-3.el7  nginx-filesystem.noarch 1:1.16.1-3.el7  nginx-mod-http-image-filter.x86_64 1:1.16.1-3.el7  nginx-mod-http-perl.x86_64 1:1.16.1-3.el7    nginx-mod-http-xslt-filter.x86_64 1:1.16.1-3.el7
  nginx-mod-mail.x86_64 1:1.16.1-3.el7      nginx-mod-stream.x86_64 1:1.16.1-3.el7   openssl11-libs.x86_64 1:1.1.1g-1.el7

Complete!

```
###### COMS: `On va démarer le service nginx, vérifier son statut et le lancer au démarrage de la machine`
```bash=
[root@rp ~]# systemctl start nginx
[root@rp ~]# systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
   Active: active (running) since Wed 2020-12-16 15:10:57 CET; 9s ago
  Process: 1884 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
  Process: 1881 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
  Process: 1880 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
 Main PID: 1886 (nginx)
   CGroup: /system.slice/nginx.service
           ├─1886 nginx: master process /usr/sbin/nginx
           └─1887 nginx: worker process

Dec 16 15:10:57 rp.tp2.cesi systemd[1]: Starting The nginx HTTP and reverse proxy server...
Dec 16 15:10:57 rp.tp2.cesi nginx[1881]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
Dec 16 15:10:57 rp.tp2.cesi nginx[1881]: nginx: configuration file /etc/nginx/nginx.conf test is successful
Dec 16 15:10:57 rp.tp2.cesi systemd[1]: Failed to parse PID from file /run/nginx.pid: Invalid argument
Dec 16 15:10:57 rp.tp2.cesi systemd[1]: Started The nginx HTTP and reverse proxy server.

[root@rp ~]# systemctl enable nginx
Created symlink from /etc/systemd/system/multi-user.target.wants/nginx.service to /usr/lib/systemd/system/nginx.service.
```


```bash=
[root@rp ~]# firewall-cmd --zone=public --permanent --add-service=http
success
[root@rp ~]# firewall-cmd --zone=public --permanent --add-service=https
success
[root@rp ~]# firewall-cmd --reload
success
```
###### tags: `On va modifier le fichier de configuration nginx et on ajoute la valeur proxy_pass http://10.55.55.12; dans location`
```bash=
[root@rp ~]# vim /etc/nginx/nginx.conf

  location / {
                proxy_pass http://10.55.55.12;
        }
```
        
###### COMS:`On va sur un navigateur web en tapant l'adresse ip du serveur proxy, on voit vient qu'il est redirigé sur le serveur web dont l'adresse ip est 10.55.55.12` 

![](https://i.imgur.com/BvF4DXF.png)


# FAIL2BAN 

```bash=
[root@rp ~]# yum install -y epel-release
[root@rp ~]# yum install fail2ban-server fail2ban-firewalld

Installed:
  fail2ban-firewalld.noarch 0:0.11.1-10.el7                    fail2ban-server.noarch 0:0.11.1-10.el7

Dependency Installed:
  systemd-python.x86_64 0:219-78.el7_9.2

Complete!

[root@rp ~]# systemctl start fail2ban
[root@rp ~]# systemctl status fail2ban
● fail2ban.service - Fail2Ban Service
   Loaded: loaded (/usr/lib/systemd/system/fail2ban.service; disabled; vendor preset: disabled)
   Active: active (running) since Thu 2020-12-17 11:14:51 CET; 4s ago
     Docs: man:fail2ban(1)
  Process: 2262 ExecStartPre=/bin/mkdir -p /run/fail2ban (code=exited, status=0/SUCCESS)
 Main PID: 2263 (f2b/server)
   CGroup: /system.slice/fail2ban.service
           └─2263 /usr/bin/python2 -s /usr/bin/fail2ban-server -xf start

Dec 17 11:14:51 rp.tp2.cesi systemd[1]: Starting Fail2Ban Service...
Dec 17 11:14:51 rp.tp2.cesi systemd[1]: Started Fail2Ban Service.
Dec 17 11:14:51 rp.tp2.cesi fail2ban-server[2263]: Server ready
[root@rp ~]# systemctl enable fail2ban
Created symlink from /etc/systemd/system/multi-user.target.wants/fail2ban.service to /usr/lib/systemd/system/fail2ban.service.
[root@rp ~]# cd /etc/fail2ban/jail.d
[root@rp jail.d]# ls
00-firewalld.conf
[root@rp jail.d]# touch sshd.local
[root@rp jail.d]# ls
00-firewalld.conf  sshd.local
[root@rp jail.d]# vim sshd.local
[DEFAULT]
bantime = 20
ignoreip =

[sshd]
enabled = true
~              
[root@rp jail.d]# systemctl restart fail2ban
[root@rp jail.d]# systemctl status fail2ban
● fail2ban.service - Fail2Ban Service
   Loaded: loaded (/usr/lib/systemd/system/fail2ban.service; enabled; vendor preset: disabled)
   Active: active (running) since Thu 2020-12-17 11:16:31 CET; 5s ago
     Docs: man:fail2ban(1)
  Process: 2298 ExecStop=/usr/bin/fail2ban-client stop (code=exited, status=0/SUCCESS)
  Process: 2300 ExecStartPre=/bin/mkdir -p /run/fail2ban (code=exited, status=0/SUCCESS)
 Main PID: 2301 (f2b/server)
   CGroup: /system.slice/fail2ban.service
           └─2301 /usr/bin/python2 -s /usr/bin/fail2ban-server -xf start

```


```bash=
[root@web jail.d]# yum install -y epel-release
[root@web jail.d]# yum install fail2ban-server fail2ban-firewalld
[root@web jail.d]# systemctl start fail2ban
[root@web jail.d]# systemctl status fail2ban
● fail2ban.service - Fail2Ban Service
   Loaded: loaded (/usr/lib/systemd/system/fail2ban.service; enabled; vendor preset: disabled)
   Active: active (running) since Thu 2020-12-17 11:00:51 CET; 5h 17min ago
     Docs: man:fail2ban(1)
 Main PID: 2572 (f2b/server)
   CGroup: /system.slice/fail2ban.service
           └─2572 /usr/bin/python2 -s /usr/bin/fail2ban-server -xf start
[root@web jail.d]# systemctl enable fail2ban
[root@web jail.d]# cd /etc/fail2ban/jail.d
[root@web jail.d]# ls
00-firewalld.conf  sshd.local
[root@web jail.d]# touch sshd.local
[root@web jail.d]# vim sshd.local

[DEFAULT]
banetime = 20
ignoreip = 10.55.55.1

[sshd]
enabled=true

[root@web jail.d]# systemctl restart fail2ban
[root@web jail.d]# systemctl status fail2ban

```
```bash=
PS C:\Users\SUPERTEK> ssh admin@10.55.55.12
admin@10.55.55.12's password:
Permission denied, please try again.
admin@10.55.55.12's password:
Permission denied, please try again.
admin@10.55.55.12's password:
admin@10.55.55.12: Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).
PS C:\Users\SUPERTEK> ssh admin@10.55.55.12
admin@10.55.55.12's password:
Permission denied, please try again.
admin@10.55.55.12's password:
Permission denied, please try again.
admin@10.55.55.12's password:
admin@10.55.55.12: Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).
PS C:\Users\SUPERTEK>
PS C:\Users\SUPERTEK> ssh admin@10.55.55.12
ssh: connect to host 10.55.55.12 port 22: Connection timed out
PS C:\Users\SUPERTEK>
```
![](https://i.imgur.com/dP1oow3.png)

![](https://i.imgur.com/5kRppx9.png)

![](https://i.imgur.com/tPdKbZH.png)

![](https://i.imgur.com/ts4TKVj.png)



# HTTPS

```bash=
[root@rp ~]# openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout web.cesi.key -out web.cesi.crt
Generating a 2048 bit RSA private key
................................................................................................+++
....................................................................................................................................................................+++
writing new private key to 'web.cesi.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:FR
State or Province Name (full name) []:BX
Locality Name (eg, city) [Default City]:BORDEAUX
Organization Name (eg, company) [Default Company Ltd]:CESI
Organizational Unit Name (eg, section) []:
Common Name (eg, your name or your server's hostname) []:web.cesi
Email Address []:lenny@kerninon.com
```
```bash=
[root@rp ~]# mv web.cesi.crt /etc/pki/tls/certs/web.cesi.crt
[root@rp ~]# mv web.cesi.key /etc/pki/tls/private/web.cesi.key

[root@rp ~]# cd /etc/pki/tls/certs/
[root@rp certs]# ls
ca-bundle.crt  ca-bundle.trust.crt  make-dummy-cert  Makefile  renew-dummy-cert  web.cesi.crt
[root@rp ~]# cd /etc/pki/tls/private
[root@rp private]# ls
web.cesi.key

[root@rp ~]# vim /etc/nginx/nginx.conf
events {}

http {
    server {
        listen       80;

        server_name web.cesi;

        return 301 https://$host$request_uri;
                   }

    server {
        listen       443 ssl;

        server_name web.cesi;



        ssl_certificate /etc/pki/tls/certs/web.cesi.crt;
        ssl_certificate_key /etc/pki/tls/private/web.cesi.key;

        location / {
            proxy_pass http://10.55.55.12;
               }
         }
}

[root@rp ~]# systemctl restart nginx
[root@rp ~]# systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor preset: disabled)
   Active: active (running) since Thu 2020-12-17 13:37:15 CET; 2s ago
  Process: 2692 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
  Process: 2689 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
  Process: 2688 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
 Main PID: 2694 (nginx)
   CGroup: /system.slice/nginx.service
           ├─2694 nginx: master process /usr/sbin/nginx
           └─2695 nginx: worker process

```
###### COMS: `MODIFIER LE FICHIER HOSTS`
```bash=
# Copyright (c) 1993-2009 Microsoft Corp.
#
# This is a sample HOSTS file used by Microsoft TCP/IP for Windows.
#
# This file contains the mappings of IP addresses to host names. Each
# entry should be kept on an individual line. The IP address should
# be placed in the first column followed by the corresponding host name.
# The IP address and the host name should be separated by at least one
# space.
#
# Additionally, comments (such as these) may be inserted on individual
# lines or following the machine name denoted by a '#' symbol.
#
# For example:
#
#      102.54.94.97     rhino.acme.com          # source server
#       38.25.63.10     x.acme.com              # x client host

# localhost name resolution is handled within DNS itself.
#	127.0.0.1       localhost
#	::1             localhost


#    10.55.55.13	web.cesi

```


![](https://i.imgur.com/e00MV4t.png)


# 3 - MONITORING
###### COMS: `On installe netdata sur nos 3 serveurs`

```bash=
[root@rp ~]# yum install -y zlib-devel libuuid-devel libmnl-devel gcc make git autoconf autogen automake pkgconfig
yum install -y curl jq nodejs
yum install -y autoconf-archive cmake elfutils-libelf-devel json-c-devel libtool libuv-devel lz4-devel nmap-ncat openssl-devel python3
firewall-cmd --permanent --add-port=19999/tcp
firewall-cmd --reload
curl https://my-netdata.io/kickstart.sh --output netdata
chmod +x netdata && bash netdata
```
###### COMS: `On attérit bien sur netdata depuis un navigateur via le port : 19999`

![](https://i.imgur.com/Nr3I76n.png)


```bash=
[root@rp ~]# /etc/netdata/edit-config health_alarm_notify.conf

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discordapp.com/api/webhooks/789129344815071232/jfmUi8K0ky7w1MraLHD1fhZbOyib1pIF-JY7rDanjR7o9R0MQmh_fu0OeRUAjPfPPP_P"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="général"
```
```bash=
[root@rp ~]# systemctl restart netdata
[root@rp health.d]# systemctl status netdata
● netdata.service - Real time performance monitoring
   Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
   Active: active (running) since Thu 2020-12-17 15:14:21 CET; 9min ago
```
   
   
```bash=
[root@rp ~]# cd /usr/lib/netdata/conf.d/health.d
You have new mail in /var/spool/mail/root
[root@rp health.d]# ls
adaptec_raid.conf  ceph.conf           exporting.conf  kubelet.conf             netfilter.conf   retroshare.conf     udp_errors.conf
am2320.conf        cgroups.conf        fping.conf      lighttpd.conf            nginx.conf       riakkv.conf         unbound.conf
anomalies.conf     cockroachdb.conf    fronius.conf    linux_power_supply.conf  nginx_plus.conf  scaleio.conf        varnish.conf
apache.conf        couchdb.conf        gearman.conf    load.conf                phpfpm.conf      softnet.conf        vcsa.conf
apcupsd.conf       cpu.conf            haproxy.conf    mdstat.conf              pihole.conf      squid.conf          vernemq.conf
apps_plugin.conf   dbengine.conf       hdfs.conf       megacli.conf             portcheck.conf   stiebeleltron.conf  vsphere.conf
backend.conf       disks.conf          httpcheck.conf  memcached.conf           postgres.conf    swap.conf           web_log.conf
bcache.conf        dnsmasq_dhcp.conf   ioping.conf     memory.conf              processes.conf   tcp_conn.conf       whoisquery.conf
beanstalkd.conf    dns_query.conf      ipc.conf        mongodb.conf             pulsar.conf      tcp_listen.conf     wmi.conf
bind_rndc.conf     dockerd.conf        ipfs.conf       mysql.conf               qos.conf         tcp_mem.conf        x509check.conf
boinc.conf         elasticsearch.conf  ipmi.conf       named.conf               ram.conf         tcp_orphans.conf    zfs.conf
btrfs.conf         entropy.conf        isc_dhcpd.conf  net.conf                 redis.conf       tcp_resets.conf     zookeeper.conf


[root@rp health.d]# vim ram.conf
alarm: ram_usage
 on: system.ram
lookup: average -1m percentage of used
 units: %
 every: 1m
 warn: $this > 75                                                                                                                             crit: $this > 90
 info: The percentage of RAM used by the system.
 ```
```bash= 
[root@rp health.d]# yum install stress
 Installed:
  stress.x86_64 0:1.0.4-16.el7

Complete!
[root@rp health.d]# stress --vm 1 --vm-bytes 800M
stress: info: [23155] dispatching hogs: 0 cpu, 0 io, 1 vm, 0 hdd
```
![](https://i.imgur.com/agEDGSO.png)

![](https://i.imgur.com/S1Q312N.png)


# BONUS 
```bash=
# Copyright (c) 1993-2009 Microsoft Corp.
#
# This is a sample HOSTS file used by Microsoft TCP/IP for Windows.
#
# This file contains the mappings of IP addresses to host names. Each
# entry should be kept on an individual line. The IP address should
# be placed in the first column followed by the corresponding host name.
# The IP address and the host name should be separated by at least one
# space.
#
# Additionally, comments (such as these) may be inserted on individual
# lines or following the machine name denoted by a '#' symbol.
#
# For example:
#
#      102.54.94.97     rhino.acme.com          # source server
#       38.25.63.10     x.acme.com              # x client host

# localhost name resolution is handled within DNS itself.
#	127.0.0.1       localhost
#	::1             localhost


10.55.55.13	web.cesi
10.55.55.13	dbmon.cesi
10.55.55.13	webmon.cesi
10.55.55.13	rpmon.cesi
```
```bash=
[root@rp ~]# rm /etc/pki/tls/certs/web.cesi.crt
rm /etc/pki/tl[root@rp ~]# rm /etc/pki/tls/private/web.cesi.key
```
```bash=
[root@rp ~]# openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout web.cesi.key -out web.cesi.crt
Generating a 2048 bit RSA private key
................................................+++
....+++
writing new private key to 'web.cesi.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:
State or Province Name (full name) []:
Locality Name (eg, city) [Default City]:
Organization Name (eg, company) [Default Company Ltd]:
Organizational Unit Name (eg, section) []:
Common Name (eg, your name or your server's hostname) []:*.cesi
Email Address []: 
```
```bash=
[root@rp ~]# cp web.cesi.crt /etc/pki/tls/certs/cesi.crt
[root@rp ~]# cp web.cesi.key /etc/pki/tls/private/cesi.key
```
```bash=
[root@rp ~]# vim /etc/nginx/nginx.conf

events {}

http {
    server {
        listen       80;

        server_name web.cesi;

        return 301 https://$host$request_uri;
           }

    server {
        listen       443 ssl;

        server_name web.cesi;

        ssl_certificate     /etc/pki/tls/certs/cesi.crt;
        ssl_certificate_key /etc/pki/tls/private/cesi.key;

        location / {
            proxy_pass   http://10.55.55.12/;
                   }

           }

    server {
        listen       443 ssl;

        server_name dbmon.cesi;

        ssl_certificate     /etc/pki/tls/certs/cesi.crt;
        ssl_certificate_key /etc/pki/tls/private/cesi.key;

        location / {
            proxy_pass   http://10.55.55.11:19999;
                   }

           }

    server {
        listen       443 ssl;

        server_name webmon.cesi;

        ssl_certificate     /etc/pki/tls/certs/cesi.crt;
        ssl_certificate_key /etc/pki/tls/private/cesi.key;

        location / {
            proxy_pass   http://10.55.55.12:19999;
                   }

           }
}
```
```bash=
[root@rp ~]# systemctl restart nginx
```
![](https://i.imgur.com/l8uQjWZ.png)

# 1. UTILISATEURS

## Prérequis 
###### COMS: `On installe vim pour l'édition de fichiers`
```bash=
[admin@localhost ~]$ sudo yum install vim 
```
###### COMS: ` On va aussi désactiver SELINUX`
```bash=
[admin@localhost ~]$ sudo setenforce 0
[admin@localhost ~]$ sudo sed -i 's/SELINUX=enforcing/SELINUX=permissive/g' /etc/selinux/config
```


## Création et configuration 

###### COMS: `l'utilisateur admin est déjà crée`
```bash=
[admin@localhost ~]$ sudo useradd admin -m -s /bin/bash -u 3000
useradd: user 'admin' already exists
[admin@localhost ~]$ ls
admin lenny
``` 
###### COMS: `On ajoute le groupe admins avec les permissions admin`
```bash=
[admin@localhost ~]$ sudo groupadd admins
groupadd: group 'admins' already exists
[admin@localhost ~]$ sudo visudo 
"%admins ALL=(ALL)       ALL"
[admin@localhost ~]$ usermod -aG admins admin

```
###### tags: `On vérifie que l'utilisateur admin fait bien partie du groupe wheel/admins`
```bash=
[admin@localhost ~]# groups admin
admin : root wheel admin admins
```

# 2. SSH
###### COMS: `Aller sur le poste client et générer la clé , on laisse le chemin par défaut et on ne met pas de passphrase pour le TP même si c'est plus secure :` 
```bash=
PS C:\Users\SUPERTEK> ssh-keygen -t rsa -b 4096
Generating public/private rsa key pair.
Enter file in which to save the key (C:\Users\SUPERTEK/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in C:\Users\SUPERTEK/.ssh/id_rsa.
Your public key has been saved in C:\Users\SUPERTEK/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:IcnMnIrRypxS6VX........................
```
###### COMS: `On se connecte sur le serveur en ssh via powershell :`
```bash=
PS C:\Users\SUPERTEK> ssh admin@10.55.55.10
```
###### COMS: `On va créer un dossier .ssh et coller la clé dans le fichier authorized_keys`
```bash=
[admin@localhost ~]$ mkdir .ssh
[admin@localhost ~]$ cd .ssh
[admin@localhost .ssh]$ echo 'ssh-rsa ' > authorized_keys
```
###### COMS: `On gère les permissions :`
```bash=
[admin@localhost .ssh]$ ls -al
total 4
drwxrwxr-x. 2 admin admin  29 Dec 15 13:32 .
drwx------. 3 admin admin  95 Dec 15 13:28 ..
-rw-rw-r--. 1 admin admin 750 Dec 15 13:32 authorized_keys
```
###### COMS: `en attribuant 700 au dossier .ssh et 600 au fichier authorized_keys :` 
```bash=
[admin@localhost .ssh]$ cd ..
[admin@localhost ~]$ chmod 700 .ssh
[admin@localhost ~]$ chmod 600 .ssh/authorized_keys
```
###### COMS: `On voit bien que les permissions ont été modifiées : `
```bash=
[root@localhost .ssh]# ls -al
total 4
drwx------. 2 1000 admin  29 Dec 15 13:32 .
drwx------. 3 1000 admin 111 Dec 15 17:03 ..
-rw-------. 1 1000 admin 750 Dec 15 13:32 authorized_keys
```
###### COMS: `On peut se connecter sans MDP :` 
```bash=
PS C:\Users\SUPERTEK> ssh admin@10.55.55.10
Last login: Tue Dec 15 13:25:42 2020 from 10.55.55.1
```


# Partie 2 : Configuration réseau 
### Définition du nom de domaine ( lenny.local ) et du serveur dns 1.1.1.1 et on vérifie bien dans le fichier que la modification à lieu. 
```bash=
[root@localhost ~]# sudo echo -e 'domain lenny.local\nnameserver 1.1.1.1' | tee /etc/resolv.conf
domain lenny.local
nameserver 1.1.1.1
```

# PARTIE 3 : LVM et Partitionnement 


###### COMS: `Montrer les disques et périphériques présents sur la machine`
```bash=
[root@localhost ~]# lsblk
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda               8:0    0   20G  0 disk
├─sda1            8:1    0    1G  0 part /boot
└─sda2            8:2    0   19G  0 part
  ├─centos-root 253:0    0   17G  0 lvm  /
  └─centos-swap 253:1    0    2G  0 lvm  [SWAP]
sdb               8:16   0    3G  0 disk
sdc               8:32   0    3G  0 disk
sr0              11:0    1  4.3G  0 rom
```
###### COMS: `On initialise nos volumes physiques sbd et sdc de  3Go `
```bash=
[root@localhost ~]# pvcreate /dev/sdb
  Physical volume "/dev/sdb" successfully created.
[root@localhost ~]# pvcreate /dev/sdc
  Physical volume "/dev/sdc" successfully created.
[root@localhost ~]# pvs
```
###### COMS: `On voit bien qu'ils sont initialisés avec la commande pvs et pvdisplay.`
```bash=
[root@localhost ~]# pvs
  PV         VG     Fmt  Attr PSize   PFree
  /dev/sda2  centos lvm2 a--  <19.00g    0
  /dev/sdb          lvm2 ---    3.00g 3.00g
  /dev/sdc          lvm2 ---    3.00g 3.00g
 
 [root@localhost ~]# sudo pvdisplay
  --- Physical volume ---
  PV Name               /dev/sda2
  VG Name               centos
  PV Size               <19.00 GiB / not usable 3.00 MiB
  Allocatable           yes (but full)
  PE Size               4.00 MiB
  Total PE              4863
  Free PE               0
  Allocated PE          4863
  PV UUID               hM4KGl-Tq5N-Y6nl-CDS0-Aeer-z10G-Ytt3Zd

  "/dev/sdb" is a new physical volume of "3.00 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sdb
  VG Name
  PV Size               3.00 GiB
  Allocatable           NO
  PE Size               0
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               Cxww6l-nqii-bvdx-aY7J-Rc05-uMT1-cxWYRu

  "/dev/sdc" is a new physical volume of "3.00 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sdc
  VG Name
  PV Size               3.00 GiB
  Allocatable           NO
  PE Size               0
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               19RSXy-KJGw-MM7g-udl6-k5HC-EbgD-Heo5Az
  ```
  ###### COMS: `On crée un volume de groupe nommé data sur le volume phisyque sdb et on voit qu'il a bien été crée avec la commande vgs`
  
  ```bash=
[root@localhost ~]# vgs
  VG     #PV #LV #SN Attr   VSize   VFree
  centos   1   2   0 wz--n- <19.00g     0
  data     1   0   0 wz--n-  <3.00g <3.00g
  ```
  ###### COMS: `On crée 3 volumes logiques nommés data-1, data-2 et data-3. On les voients vien crées avec la commande lvdisplay.` 
  
  ```bash=
  [root@localhost ~]# lvdisplay
  --- Logical volume ---
  LV Path                /dev/centos/swap
  LV Name                swap
  VG Name                centos
  LV UUID                Zefsl3-M4cs-lyqe-L6Bn-NZZ0-KKqR-6H5W3I
  LV Write Access        read/write
  LV Creation host, time localhost, 2020-12-14 16:35:59 +0100
  LV Status              available
  # open                 2
  LV Size                2.00 GiB
  Current LE             512
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:1

  --- Logical volume ---
  LV Path                /dev/centos/root
  LV Name                root
  VG Name                centos
  LV UUID                QIo16w-sib5-D6dh-Cdmp-B7eH-kxcf-3blmwf
  LV Write Access        read/write
  LV Creation host, time localhost, 2020-12-14 16:36:00 +0100
  LV Status              available
  # open                 1
  LV Size                <17.00 GiB
  Current LE             4351
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:0

  --- Logical volume ---
  LV Path                /dev/data/data-1
  LV Name                data-1
  VG Name                data
  LV UUID                4z7MOJ-Lrux-BAMX-mVIz-7puj-GMgj-10Yi1D
  LV Write Access        read/write
  LV Creation host, time localhost.localdomain, 2020-12-15 14:48:21 +0100
  LV Status              available
  # open                 0
  LV Size                1.00 GiB
  Current LE             256
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:2

  --- Logical volume ---
  LV Path                /dev/data/data-2
  LV Name                data-2
  VG Name                data
  LV UUID                SKD88H-qeDv-JKdK-GLzD-bACC-DS7p-ocuf2E
  LV Write Access        read/write
  LV Creation host, time localhost.localdomain, 2020-12-15 14:48:48 +0100
  LV Status              available
  # open                 0
  LV Size                1.00 GiB
  Current LE             256
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:3

  --- Logical volume ---
  LV Path                /dev/data/data-3
  LV Name                data-3
  VG Name                data
  LV UUID                Ck9Uz5-2Klq-IUho-jzkV-1dlH-FoJw-bJWHqp
  LV Write Access        read/write
  LV Creation host, time localhost.localdomain, 2020-12-15 14:48:54 +0100
  LV Status              available
  # open                 0
  LV Size                1.00 GiB
  Current LE             256
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:4
  ```
  
  ###### COMS: `On formate les partions en ext4.` 
  
  ```bash=
  [root@localhost ~]# mkfs -t ext4 /dev/data/data-3
mke2fs 1.42.9 (28-Dec-2013)
Filesystem label=
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
Stride=0 blocks, Stripe width=0 blocks
65536 inodes, 262144 blocks
13107 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=268435456
8 block groups
32768 blocks per group, 32768 fragments per group
8192 inodes per group
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done

[root@localhost ~]# mkfs -t ext4 /dev/data/data-2
mke2fs 1.42.9 (28-Dec-2013)
Filesystem label=
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
Stride=0 blocks, Stripe width=0 blocks
65536 inodes, 262144 blocks
13107 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=268435456
8 block groups
32768 blocks per group, 32768 fragments per group
8192 inodes per group
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done

[root@localhost ~]# mkfs -t ext4 /dev/data/data-1
mke2fs 1.42.9 (28-Dec-2013)
Filesystem label=
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
Stride=0 blocks, Stripe width=0 blocks
65536 inodes, 262144 blocks
13107 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=268435456
8 block groups
32768 blocks per group, 32768 fragments per group
8192 inodes per group
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done
```
### On monte les partitions pour qu'elles soient accessibles aux points de montage /mnt/data-01, /mnt/data-02 et /mnt/data-02. Et on vérifie qu'elles soient bien montées. 
```bash=
[root@localhost ~]# mkdir /mnt/data-01
[root@localhost ~]# mkdir /mnt/data-02
[root@localhost ~]# mkdir /mnt/data-03
[root@localhost ~]# ls /mnt
data-01  data-02  data-03  hgfs
```
###### COMS: `On monte les dossiers nommés data-01 à 03`
```bash=
[root@localhost ~]# sudo mount /dev/data/data-1 /mnt/data-01
```
###### COMS: `On vérifie qu'ils sont bien présents avec la commande df -h`
```bash=
[root@localhost ~]# df -h
Filesystem                Size  Used Avail Use% Mounted on
devtmpfs                  475M     0  475M   0% /dev
tmpfs                     487M     0  487M   0% /dev/shm
tmpfs                     487M  7.7M  479M   2% /run
tmpfs                     487M     0  487M   0% /sys/fs/cgroup
/dev/mapper/centos-root    17G  1.5G   16G   9% /
/dev/sda1                1014M  163M  852M  17% /boot
tmpfs                      98M     0   98M   0% /run/user/0
/dev/mapper/data-data--1  976M  2.6M  907M   1% /mnt/data-01
/dev/mapper/data-data--2  976M  2.6M  907M   1% /mnt/data-02
[root@localhost ~]# sudo mount /dev/data/data-3 /mnt/data-03
```
###### COMS: `On va modifier le fichier fstab pour que les partitions soient montées automatiquement au démarrage du système`
```bash=
/dev/data/data-1         /mnt/data-01           ext4    defaults        0 0
/dev/data/data-2         /mnt/data-02           ext4    defaults        0 0
/dev/data/data-3         /mnt/data-03           ext4    defaults        0 0
~                                                                          
```
###### COMS: `On peut vérifier les volumes logiques avec lvdisplay`
```bash=
[root@localhost ~]# lvdisplay

 --- Logical volume ---
  LV Path                /dev/data/data-1
  LV Name                data-1
  VG Name                data
  LV UUID                4z7MOJ-Lrux-BAMX-mVIz-7puj-GMgj-10Yi1D
  LV Write Access        read/write
  LV Creation host, time localhost.localdomain, 2020-12-15 14:48:21 +0100
  LV Status              available
  # open                 1
  LV Size                1.00 GiB
  Current LE             256
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:2
  ```
  # On reboot !
  
  #  4 - GESTION DES SERVICES 
  
  ###### COMS: `On vérifie que checker si le services firewall est en cours de fonctionnement et actif avec ces commandes : `
  ```bash=
  [root@localhost ~]# systemctl is-active firewalld
active
  [root@localhost ~]# systemctl status firewalld
● firewalld.service - firewalld - dynamic firewall daemon
   Loaded: loaded (/usr/lib/systemd/system/firewalld.service; enabled; vendor preset: enabled)
   Active: active (running) since Tue 2020-12-15 15:09:33 CET; 8min ago
     Docs: man:firewalld(1)
 Main PID: 792 (firewalld)
   CGroup: /system.slice/firewalld.service
           └─792 /usr/bin/python2 -Es /usr/sbin/firewalld --nofork --nopid
``` 
    
        
# 2 . CREATION DE SERVICE
           
###### COMS: `On crée ensuite le fichier web.service dans le dossier system`

```bash=

[root@localhost ~]# touch /etc/systemd/system/web.service

```
###### COMS: `On édite ce fichier avec vim et on rajoute ces 2 arguments dans le fichier sous services :` 

```bash=
[root@localhost ~]# vim /etc/systemd/system/web.service
```
```bash=
WorkingDirectory=/srv/webserver
user=web
```
###### COMS: `Enfin on ouvre le port 8888 pour l'accès au web` 
```bash=
[root@localhost ~]# firewall-cmd --add-port=8888/tcp --permanent
success
```
###### COMS: `Une fois l'unité de service créée, il faut demander à systemd de relire les fichiers de configuration` 
```bash=
[root@localhost ~]# systemctl daemon-reload
[root@localhost ~]# firewall-cmd --reload
success
```

###### COMS: `On peut vérifier et intéragir avec le service à l'aide des 3 commandes suivantes ,start pour démarrer le service, status pour checker si le service est démarré et bien actif et enable pour démarrer le service au démarrage de la machine: `
```bash=
[root@localhost ~]# systemctl status web
● web.service - Very simple web service
   Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
   Active: inactive (dead)
[root@localhost ~]# systemctl start web
[root@localhost ~]# systemctl enable web
Created symlink from /etc/systemd/system/multi-user.target.wants/web.service to /etc/systemd/system/web.service.
[root@localhost ~]# systemctl status web
● web.service - Very simple web service
   Loaded: loaded (/etc/systemd/system/web.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2020-12-15 15:46:26 CET; 34s ago
 Main PID: 1604 (python2)
   CGroup: /system.slice/web.service
           └─1604 /bin/python2 -m SimpleHTTPServer 8888

Dec 15 15:46:26 localhost.localdomain systemd[1]: Started Very simple web service.
```
# B . MODIFICATION DE L'UNITE

## On crée un utilisateur WEB : 
```bash=
[root@localhost ~]# useradd web -p web -d /home/web -s /bin/bash
```
###### COMS: `L'utilisateur Web devient le propriétaire du dossier webserver :`
```bash=
[root@localhost ~]# chown web:web /srv/webserver
```
###### COMS: `On va venir créer un fichier texte salut et voir si il est dispo sur la plateforme Web : `
```bash=
[root@localhost ~]# echo 'SALUT' > /srv/webserver/SALUT.txt
[root@localhost ~]# ls -al /srv
total 0
drwxr-xr-x.  3 root root  23 Dec 15 15:56 .
dr-xr-xr-x. 17 root root 224 Dec 14 16:39 ..
drwxr-xr-x.  2 web  web   23 Dec 15 16:00 webserver
[root@localhost ~]# ls -al /srv/webserver
total 4
drwxr-xr-x. 2 web  web  23 Dec 15 16:00 .
drwxr-xr-x. 3 root root 23 Dec 15 15:56 ..
-rw-r--r--. 1 root root  6 Dec 15 16:00 SALUT.txt
```
![](https://i.imgur.com/eLKwGcW.png)
